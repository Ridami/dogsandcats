package ru.example.ridami.dogsandcats.ui;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import ru.example.ridami.dogsandcats.R;


public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    TabLayout tabLayout;
    private final static String TAB_POSITION = "TAB_POSITION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initTabLayout();

        if(savedInstanceState == null)
            switchFragment(getString(R.string.title_tab_left), 0);


    }

    private void initTabLayout() {
        tabLayout = findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.title_tab_left), true);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.title_tab_right));
        tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(TAB_POSITION, tabLayout.getSelectedTabPosition());

        super.onSaveInstanceState(outState);

    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int position = savedInstanceState.getInt(TAB_POSITION, 0);
        tabLayout.getTabAt(position).select();

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switchFragment(tab.getText().toString(), tab.getPosition());

    }

    private void switchFragment(String name, int index) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(name);

        if (fragment == null ){
            fragment = MainFragment.newInstance(name, index);
        }

        fragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, fragment, name)
                .commit();

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override public void onBackPressed() {
       // super.onBackPressed();
        finish();
    }
}
