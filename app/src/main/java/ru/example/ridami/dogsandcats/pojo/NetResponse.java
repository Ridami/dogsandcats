package ru.example.ridami.dogsandcats.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ridami on 21.01.2018.
 */

public class NetResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<ElementOfList> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ElementOfList> getData() {
        return data;
    }

    public void setData(List<ElementOfList> data) {
        this.data = data;
    }

}