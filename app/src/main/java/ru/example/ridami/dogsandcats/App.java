package ru.example.ridami.dogsandcats;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.example.ridami.dogsandcats.api.Api;

/**
 * Created by Ridami on 21.01.2018.
 */

public class App extends Application {
    private static Api api;


    @Override
    public void onCreate() {
        super.onCreate();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://kot3.com/xim/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(Api.class);
    }

    public static Api getApi(){
        return api;
    }
}
