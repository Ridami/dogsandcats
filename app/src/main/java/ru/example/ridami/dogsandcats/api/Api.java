package ru.example.ridami.dogsandcats.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.example.ridami.dogsandcats.pojo.NetResponse;

/**
 * Created by Ridami on 21.01.2018.
 */

public interface Api {

    @GET("api.php")
    //http://kot3.com/xim/api.php?query=dog
    Call<NetResponse> getData(@Query("query") String query);


}
