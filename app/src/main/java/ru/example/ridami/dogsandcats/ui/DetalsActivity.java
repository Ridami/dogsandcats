package ru.example.ridami.dogsandcats.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ru.example.ridami.dogsandcats.R;

public class DetalsActivity extends AppCompatActivity {

    public static final String TITLE = "TITLE";
    public static final String IMAGE_URL = "IMAGE_URL";
    public static final String TEXT = "TEXT";
    private String  title, briefText, imageURL;
    TextView tvBriefText, tvTitle;
    ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detals);

        getExtraDetals();
        initViews();

        setViewsData();
    }

    private void setViewsData() {
        tvTitle.setText(title);
        tvBriefText.setText(briefText);
        Picasso
                .with(getBaseContext())
                .load(imageURL)
                .into(ivImage);
    }

    private void initViews() {
        ivImage = findViewById(R.id.detal_image);
        tvTitle = findViewById(R.id.detal_title);
        tvBriefText = findViewById(R.id.detal_brief_info);
    }

    private void getExtraDetals() {
        Intent intent = getIntent();
        if (intent != null){
            title = getIntent().getStringExtra(TITLE);
            imageURL = getIntent().getStringExtra(IMAGE_URL);
            briefText = getIntent().getStringExtra(TEXT);
        }
    }
}
