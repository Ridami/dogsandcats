package ru.example.ridami.dogsandcats.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.example.ridami.dogsandcats.R;
import ru.example.ridami.dogsandcats.pojo.ElementOfList;

/**
 * Created by Ridami on 21.01.2018.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
  private List<ElementOfList> list;

  public ListAdapter(List<ElementOfList> listOfElements) {
    this.list = listOfElements;
  }

  public void setList(List<ElementOfList> list) {
    this.list = list;
  }

  @Override public void onViewDetachedFromWindow(ViewHolder holder) {
    super.onViewDetachedFromWindow(holder);
  }

  @Override public void onViewRecycled(ViewHolder holder) {
    super.onViewRecycled(holder);
    holder.cleanup();
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_main, parent, false);

    ViewHolder vh = new ViewHolder(v, parent.getContext());
    return vh;
  }

  @Override public void onBindViewHolder(ViewHolder vh, int position) {
    vh.addData(list.get(position));
  }

  @Override public int getItemCount() {
    return (list == null)? 0 : list.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    ImageView ivImage;
    TextView tvTitle;
    TextView tvBriefInfo;
    Context context;
    ElementOfList data;

    public ViewHolder(View itemView, Context context) {
      super(itemView);
      ivImage = itemView.findViewById(R.id.preview_img);
      tvTitle = itemView.findViewById(R.id.title);
      tvBriefInfo = itemView.findViewById(R.id.brief_info);

      itemView.setOnClickListener(this);

      this.context = context;
    }

    public void addData(ElementOfList data) {
      this.data = data;
      Picasso.with(context).load(data.getUrl()).centerCrop().fit().into(ivImage);
      tvTitle.setText(data.getTitle());
      tvBriefInfo.setText(R.string.briefDataStab);
    }

    @Override public void onClick(View view) {
      Intent intent = new Intent(context, DetalsActivity.class);
      intent.putExtra(DetalsActivity.TITLE, data.getTitle());
      intent.putExtra(DetalsActivity.TEXT, tvBriefInfo.getText());
      intent.putExtra(DetalsActivity.IMAGE_URL, data.getUrl());

      context.startActivity(intent);
    }

    public void cleanup() {
      Picasso.with(ivImage.getContext()).cancelRequest(ivImage);
      ivImage.setImageDrawable(null);
    }
  }
}
