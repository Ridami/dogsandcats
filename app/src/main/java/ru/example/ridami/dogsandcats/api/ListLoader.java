package ru.example.ridami.dogsandcats.api;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.example.ridami.dogsandcats.App;
import ru.example.ridami.dogsandcats.pojo.ElementOfList;
import ru.example.ridami.dogsandcats.pojo.NetResponse;

/**
 * Created by Ridami on 22.01.2018.
 */

public class ListLoader extends Loader<List<ElementOfList>> {
    private final Call<NetResponse> listOfElementsCall;
    @Nullable
    private List<ElementOfList> dataList;

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (dataList != null){
            deliverResult(dataList);
        } else {
            forceLoad();
        }

    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        try {

            listOfElementsCall.clone().enqueue(new Callback<NetResponse>() {
                public void onResponse(Call<NetResponse> call, Response<NetResponse> response) {
                    dataList = new ArrayList<>();
                    if (response != null) {
                        dataList.addAll(response.body().getData());
                        deliverResult(dataList);
                    }
                }

                @Override
                public void onFailure(Call<NetResponse> call, Throwable t) {
                    Toast.makeText(getContext(), "Loading error", Toast.LENGTH_SHORT).show();
                    deliverResult(null);
                }
            });
        } catch (IllegalStateException e){
            listOfElementsCall.cancel();

            Toast.makeText(getContext(), "We waiting for internet " +
                  "connection or  dataSet loading. Wait it with us, please.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *  @param context used to retrieve the application context.
     * @param query

     */
    public ListLoader(@NonNull Context context, @NonNull String query) {
        super(context);
        listOfElementsCall = App.getApi().getData(query);

    }

    @Override
    protected void onStopLoading() {
        listOfElementsCall.cancel();
        super.onStopLoading();
    }
}
