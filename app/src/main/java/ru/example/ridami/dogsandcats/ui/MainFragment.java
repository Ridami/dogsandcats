package ru.example.ridami.dogsandcats.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import ru.example.ridami.dogsandcats.R;
import ru.example.ridami.dogsandcats.api.ListLoader;
import ru.example.ridami.dogsandcats.pojo.ElementOfList;

/**
 * Created by Ridami on 21.01.2018.
 */

public class MainFragment extends Fragment {

  public final String TAG = getClass().getSimpleName();

  private static final String ARG_INDEX = "ARG_INDEX";
  public static final  String ARG_NAME = "FRAGMENT_NAME";
  private static final String SAVE_RECYCLE_VIEW_ON_ROTATE = "SAVE_RECYCLE_VIEW_ON_ROTATE";
  private String mName;
  private int mIndex;
  private RecyclerView rv;
  private List<ElementOfList> listWithData;
  private LinearLayoutManager llManager;
  private ListAdapter adapter;

  public static MainFragment newInstance(String name, int mIndex) {

    Bundle args = new Bundle();
    args.putString(ARG_NAME, name);
    args.putInt(ARG_INDEX, mIndex);

    MainFragment fragment = new MainFragment();
    fragment.setArguments(args);

    return fragment;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle args = getArguments();

    if (args != null) {
      mName = args.getString(ARG_NAME);
      mIndex = args.getInt(ARG_INDEX);

      Bundle bundle = new Bundle();
      bundle.putString(ARG_NAME, mName);
      LoaderManager.LoaderCallbacks<List<ElementOfList>> callbacks = new DataLoadCallback();
      getLoaderManager().initLoader(mIndex, bundle, callbacks);
    }
  }

  @Override public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
    super.onViewStateRestored(savedInstanceState);
    if (savedInstanceState != null) {
      Parcelable state = savedInstanceState.getParcelable(SAVE_RECYCLE_VIEW_ON_ROTATE);
      llManager.onRestoreInstanceState(state);
    }
  }

  @Override public void onSaveInstanceState(@NonNull Bundle outState) {
    outState.putParcelable(SAVE_RECYCLE_VIEW_ON_ROTATE, llManager.onSaveInstanceState());

    super.onSaveInstanceState(outState);
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    initRecyclerView(view);
  }

  private void initRecyclerView(@NonNull View view) {
    rv = view.findViewById(R.id.my_recycler_view);
    llManager = new LinearLayoutManager(getContext());

    listWithData = new ArrayList<>();
    adapter = new ListAdapter(listWithData);

    rv.setLayoutManager(llManager);
    rv.setAdapter(adapter);
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  @Nullable @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);
    return view;
  }

  private class DataLoadCallback implements LoaderManager.LoaderCallbacks<List<ElementOfList>> {

    @Override public Loader<List<ElementOfList>> onCreateLoader(int id, Bundle args) {
      ListLoader loader = null;
      String query = "";
      if (args != null) query = args.getString(ARG_NAME);
      if (id == mIndex) {
        loader = new ListLoader(getContext(), query);
      }
      return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<ElementOfList>> loader, List<ElementOfList> data) {

      ((ListAdapter) rv.getAdapter()).setList(data);
      rv.getAdapter().notifyDataSetChanged();
    }

    @Override public void onLoaderReset(Loader<List<ElementOfList>> loader) {
      Log.i(TAG, "onLoaderReset: ");
    }
  }
}
